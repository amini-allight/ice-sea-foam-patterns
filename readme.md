# Ice Sea Foam Patterns

These are cutting patterns for the foam pads used in the Ice Sea VR headset. There are two foam pads, one at the front of the headset and one at the back, which are slightly different shapes.

## Usage

1. The pattern should be printed on A4 or Letter-sized paper. A different image is included in this repository for each paper size, make sure you are using the correct one.
2. The image **must** be printed edge-to-edge with no border. Your printer may warn this will cause some of the image to be cut off: ignore it.
3. If your printer can print right to the edge of the page the corner crosses should touch the edge of the paper. If it cannot part of them will be cut off, this is fine.
4. After printing cut carefully around the two curved shapes in the center of the page.
5. Pin the cut out shapes with sewing pins to the top of a 40 mm deep block of foam. Try to angle the pin inwards so they don't get in the way of cutting around the edge of the shape.
6. Cut carefully around the edge of the shapes, all the way through the foam.
7. Insert the larger foam block into the rear cushion holder of your headstrap and the smaller one into the front cushion holder.

## Credit & License

Created by Amini Allight, licensed under Creative Commons Attribution-Non-Commercial-ShareAlike 4.0.
